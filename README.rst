##############
runit-services
##############

Runit init configuration for VectorLinux. It features process supervision,
logging, services startup in parallel between others.
It starts the system using three stages. In stage 1 runit runs the one time
scripts, for tasks like mounting the filesystems and running checks.
Stage 2 starts the system daemons. The *runit* command drives this stage,
it is started with PID 1 until the system is down. Stage 3 controls the
system shutdown.

Install
=======

Get the runit package from vlcore repository for 7.1.
This will install the binary files. Then install this package for the 
configuration:

    make install

For installing in an alternative root:

    make install DESTDIR=<root>

Runlevels
=========

There are 3 runlevels: *safe* starts *syslog* and *crond* plus 6 instances
of *mingetty*. The *tui* runlevel adds common services on top of *safe*.
The _desktop_ runlevel adds the necesary bits for starting
an X Desktop (dbus and friends and GDM.)
For switching runlevels use *runsvchdir*.

Services
========

Runit uses one directory for each daemon. They are located in */etc/sv*.
The run file inside the service directory controls the daemon startup.
The daemon needs to run in the foreground and its output is logged to
*/var/log*.
Services can be controlled through the *sv* command line tool:

        sv start <service name>

For enabling a service create a symlink from the service directory in /etc/sv
to the proper runlevel directory. For example, this enables *sshd* in the 
*desktop* runlevel:

        ln -s /etc/sv/sshd /etc/runit/runsvdir/desktop

Reboot and Shutdown
===================

Use *init 0* for shutdown and *init 6* for reboot.

Using with sysvinit
===================

You can try runit without removing sysvinit. Boot the system passing 
init=/sbin/runit-init to the kernel. Do not use the initrd since it needs to 
be modified for runit. With LiLo you can hit *tab* for achieving this. If you 
use Grub, select the kernel you want to boot and press *e*. Add the option to
the *linux* line.

Documentation
=============

The runit package installs documentation into */usr/doc*.
You can also visit the runit home page: http://smarden.org/runit/.
Runit was written by Gerrit Pape.

