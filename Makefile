install:
	#copy basic configuration
	mkdir -p ${DESTDIR}/etc
	cp -a ./runit ${DESTDIR}/etc
	cp -a ./sv ${DESTDIR}/etc
	#runlevels
	mkdir -p ${DESTDIR}/etc/runit/runsvdir/desktop
	mkdir -p ${DESTDIR}/etc/runit/runsvdir/tui
	mkdir -p ${DESTDIR}/etc/runit/runsvdir/safe
	#desktop runlevel
	ln -s /etc/sv/acpid ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/avahidaemon ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/avahidnsconfd ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/consolekit ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/crond ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/cupsd ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/gdm ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/messagebus ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/networkmanager ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/syslog ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/sshd ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/tty2 ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/tty3 ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/tty4 ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/tty5 ${DESTDIR}/etc/runit/runsvdir/desktop
	ln -s /etc/sv/tty6 ${DESTDIR}/etc/runit/runsvdir/desktop
	#tui runlevel
	ln -s /etc/sv/acpid ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/avahidaemon ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/avahidnsconfd ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/consolekit ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/crond ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/cupsd ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/messagebus ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/networkmanager ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/syslog ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/sshd ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/tty2 ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/tty3 ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/tty4 ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/tty5 ${DESTDIR}/etc/runit/runsvdir/tui
	ln -s /etc/sv/tty6 ${DESTDIR}/etc/runit/runsvdir/tui
	#safe runlevel
	ln -s /etc/sv/acpid ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/crond ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/syslog ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/tty2 ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/tty3 ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/tty4 ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/tty5 ${DESTDIR}/etc/runit/runsvdir/safe
	ln -s /etc/sv/tty6 ${DESTDIR}/etc/runit/runsvdir/safe
	#set default runlevel
	ln -s /etc/runit/runsvdir/desktop ${DESTDIR}/etc/runit/runsvdir/current
	#enable runlevels support
	ln -s /etc/runit/runsvdir/current/ ${DESTDIR}/service

uninstall:
	-rm -r ${DESTDIR}/service
	-rm -r ${DESTDIR}/etc/sv
	-rm -r ${DESTDIR}/etc/runit

